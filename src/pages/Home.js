import {Fragment} from 'react';
import Banner from '../components/Banner';
import LandingPage from '../components/LandingPage';



export default function Home(){

	const data = {
	    
	    content: "We are here to provide quality products for everyone, everywhere",
	    destination: "/products",
	    
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<LandingPage/>
		
		</Fragment>
	)
}