import { Fragment, useEffect, useState } from 'react';
import ProductDetails from '../components/ProductDetails';
// import courses from '../data/courses';

export default function Products() {
	// Checks to see if the mockdata was captured
		/*console.log(courses);
		console.log(courses[0]);*/

	// State that will be used to store the courses retrieved from the database
	const [ products, setProducts] = useState([]);


	// Retrieve the courses from the database upon initial render of the 'Courses' component
	useEffect(() => {
		fetch('http://localhost:3000/api/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the 'courses' state to map the data retrieved from the fetch request into several 'CourseCard' components
			setProducts(
				data.map(product => {
					return (
				//key = identify documents
						<ProductDetails key={product._id} productProp={product} />
					);
				})
			)
		})
	}, []);

		
	return (
		<Fragment>
			{products}
		</Fragment>

	)
}