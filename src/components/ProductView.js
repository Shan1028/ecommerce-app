import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const history = useHistory();

	
	const { productId } = useParams();

	const [title, setTitle] = useState("");
	const [desc, setDesc] = useState("");
	const [price, setPrice] = useState(0);

	
	const order = (productId) => {

		fetch('http://localhost:3000/api/users/order', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data === true) {
					Swal.fire({
						title: "Successfully added",
						icon: "success",
						text: "You have successfully added this product."
					})
					history.push("/products");
				}
				else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}
			})

	}

	// The useEffect hook is used to check if the courseId is retrieved properly
	useEffect(() => {
		console.log(productId);

		fetch(`http://localhost:3000/api/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setTitle(data.title);
				setDesc(data.desc);
				setPrice(data.price);
			})

	}, [productId]);

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="mb-2">
						<Card.Body className="text-center">
							<Card.Title>{title}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{desc}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{
								user.id !== null ?
									<Button variant="primary" onClick={() => order(productId)} >Add to Cart</Button>
									:
									<Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>
							}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
