// import the Row, Col and card components from reat-bootstrap
import { Row, Col, Card, Button, Container } from 'react-bootstrap';

export default function LandingPage() {
    
	return (
    <Container class='text-center'>
        <Row  className = 'mt-3 mb-3'>
            <Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/ceravemoisture.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/eyebrowpencil.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairmask.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
        </Row>
        <Row  className = 'mt-3 mb-3'>
            <Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/ceravemoisture.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/eyebrowpencil.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairmask.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
        </Row>
		<Row  className = 'mt-3 mb-3'>
            <Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairmoisturizer.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairtreat.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/jojobaoil.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                    <Button variant="primary" >Add To Cart</Button>
                    </Card.Body>
                </Card>
			</Col>
        </Row>
    </Container>
        
    
		

        
	)
}