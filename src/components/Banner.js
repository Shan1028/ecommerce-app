import { Button } from 'bootstrap';
import { Row, Col} from 'react-bootstrap';




export default function Banner({data}){

	console.log(data);
	const { content, destination} = data;

	return (
		<Row>
			<Col className="p-5 text-center">
				<h1>ShanStyle</h1>
				<p>{content}</p>
			</Col>
		</Row>
	)
}

