// Import the Fragment component from react
// this will disregard the comma needed in multiple components
// import { Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
// import Err from './pages/Err';
import './App.css';
import { UserProvider } from './UserContext';


function App() {
  // State hook for the user state that's defined for a global scope
  // Initialize an object with properties from the localStorage
  const [ user, setUser ] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  }

// Used to check if the user info is properly stored upon login and the localStorage information is cleared upon logout
useEffect(() => {
  console.log(user);
  console.log(localStorage);
},[user])

  return (
  // order of components matter
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
      <Container>
        <Switch>
          <Route exact path = '/' component = {Home} />
          <Route exact path = '/cart' component = {Cart} />
          <Route exact path = '/products/:productId' component = {ProductView} />
          <Route exact path = '/login' component = {Login} />
          <Route exact path = '/logout' component = {Logout} />
          <Route exact path = '/register' component = {Register} />
          {/* <Route component = {Err} /> */}
        </Switch>
      </Container>
    </Router> 
  </UserProvider>
  );
}

export default App;
